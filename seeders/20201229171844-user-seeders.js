'use strict';

const bcrypt = require('bcrypt')

module.exports = {
  up: async (queryInterface, Sequelize) => {
     await queryInterface.bulkInsert('users', [
      {
       name: 'diko',
       profession: 'frontend developer',
       email: 'cikodiko12@gmail.com',
       role: 'admin',
       password: await bcrypt.hash('admin4321', 10),
       created_at: new Date(),
       updated_at: new Date(),
      },
      {
        name: 'indri',
        profession: 'designer',
        email: 'thary25@gmail.com',
        role: 'student',
        password: await bcrypt.hash('admin4321', 10),
        created_at: new Date(),
        updated_at: new Date(),
       },
    ], {});
  },

  // untuk mengahpapus database users
  down: async (queryInterface, Sequelize) => {
     await queryInterface.bulkDelete('users', null, {});
  }
};
