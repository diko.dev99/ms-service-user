const Validator = require('fastest-validator')
const bcrypt = require('bcrypt')
const v = new Validator()
const { User } = require('../../../models')

module.exports = async (req, res) => {
    // if validation success
    const schema = {
        name: 'string|empty:false',
        email: 'email|empty:false',
        password: 'string|min:6',
        profession: 'string|optional',
        avatar: 'string|optional',
    }

    const validate = v.validate(req.body, schema)
    if(validate.length) {
        return res.status(400).json({
            status: 'error',
            message: validate
        })
    }
    //check if any user in database
    const id = req.params.id
    const user = await User.findByPk(id)
    if(!user) {
        return res.status(404).json({
            status: 'error',
            message: 'user not found!'
        })
    }
    // enshure email not duplicate with others user
    const email = req.body.email
    if(email) {
        const checkEmail = await User.findOne({
            where: { email }
        })
        if(checkEmail && email !== user.email) {
            return res.status(409).json({
                status: 'error',
                message: 'email already exist'
            })
        }
    }

    const password = await bcrypt.hash(req.body.password, 10)
    const {
        name,
        profession,
        avatar
    } = req.body
    // updated user
    await user.update({
        email,
        password,
        name,
        profession,
        avatar
    })
    // return if success updated
    return res.json({
        status: 'success',
        data: {
            id: user.id,
            name,
            email,
            profession,
            avatar
        }
    })
}