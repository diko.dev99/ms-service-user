const express = require('express');
const router = express.Router();

const handlerRegister = require('../routes/handler/users')

router.post('/register', handlerRegister.register);
router.post('/login', handlerRegister.login);
router.post('/logout', handlerRegister.logout);
router.put('/:id', handlerRegister.update);
router.get('/:id', handlerRegister.getUser);
router.get('/', handlerRegister.getListUser);

module.exports = router;
